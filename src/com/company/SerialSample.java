package com.company;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import javax.comm.*;
import java.net.HttpURLConnection;
import java.net.URL;

class SerialSample {
    static boolean doorlock_value;
    static boolean power_value;
    static boolean dimm_value;
    static int dimm_level;

    public SerialSample() {
        super();
    }

    public static void main(String[] args) {
        try {
            (new SerialSample()).connect("COM3");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void connect(String portName) throws Exception {
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);

        if (portIdentifier.isCurrentlyOwned()) {
            System.out.println("Error: Port is currently in use");
        } else {
            CommPort commPort = portIdentifier.open(this.getClass().getName(), 2000);

            if (commPort instanceof SerialPort) {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(57600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);

                InputStream in = serialPort.getInputStream();
                OutputStream out = serialPort.getOutputStream();

                (new Thread(new SerialReader(in))).start();
                (new Thread(new SerialWriter(out))).start();
                (new Thread(new ServerPolling(out))).start();

            } else {
                System.out.println("Error: Only serial ports are handled by this example.");
            }
        }
    }

    public static class SerialReader implements Runnable {    // 노드에서 message 받아옴
        InputStream in;

        public SerialReader(InputStream in) {
            this.in = in;
        }

        public void run() {
            System.out.println("SerialReader ");
            byte[] buffer = new byte[28];
            int len = -1;
            try {
                while ((len = this.in.read(buffer)) > -1) {
                    for (int i = 0; i < len; i++)
                        System.out.printf("%X ", buffer[i]);
                    System.out.println();

                    if (buffer[10] == 0x01) {
                        System.out.println("움직임 감지");
                        NetRequest.Motion();
                        buffer[10] = 0x00;
                    } else if (buffer[10] == 0x65) {
                        if (buffer[12] == 0x01) {
                            System.out.println("Gas 누출");
                            NetRequest.Gas_Abnormal();
                        } else {
                            System.out.println("Gas 누출 후 정상");
                            NetRequest.Gas_Normal();
                        }
                        buffer[10] = 0x00;
                    }
                }

                this.in.read(buffer);
                while (this.in.available() > 0) {
                    for (int i = 0; i < buffer.length; i++)
                        System.out.printf("%X ", buffer[i]);
                    System.out.println();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class SerialWriter implements Runnable {    // 노드에 message 보냄
        OutputStream out;

        byte[] gas_detech = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0x65,
                (byte) 0x03, (byte) 0x01, (byte) 0x00, (byte) 0x51,
                (byte) 0xD1, (byte) 0x7E
        };

        byte[] gas_normal = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0x65,
                (byte) 0x03, (byte) 0x02, (byte) 0x00, (byte) 0x02,
                (byte) 0x84, (byte) 0x7E
        };

        byte[] motion = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0x01,
                (byte) 0x03, (byte) 0x03, (byte) 0x00, (byte) 0x10,
                (byte) 0x24, (byte) 0x7E
        };

        byte[] doorLock = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x06, (byte) 0x20, (byte) 0x09, (byte) 0x1F,
                (byte) 0x02, (byte) 0x0E, (byte) 0x02, (byte) 0xF4,
                (byte) 0x01, (byte) 0xC0, (byte) 0xB6, (byte) 0x7E
        };


        byte[] power_off = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0x3D,
                (byte) 0x02, (byte) 0x0D, (byte) 0x00, (byte) 0xF4,
                (byte) 0x53, (byte) 0x7E
        };

        byte[] power_on = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x06, (byte) 0x20, (byte) 0x09, (byte) 0x3D,
                (byte) 0x02, (byte) 0x0E, (byte) 0x02, (byte) 0xFF,
                (byte) 0xFF, (byte) 0xA3, (byte) 0xDA, (byte) 0x7E
        };

        byte[] dimm_down = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x15, (byte) 0x00, (byte) 0xD5,
                (byte) 0x81, (byte) 0x7E
        };

        byte[] dimm_up = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x16, (byte) 0x00, (byte) 0x86,
                (byte) 0xD4, (byte) 0x7E
        };

        byte[] dimm_off = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x71, (byte) 0x00, (byte) 0x3B,
                (byte) 0x46, (byte) 0x7E
        };

        byte[] dimm_on = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x70, (byte) 0x00, (byte) 0x0A,
                (byte) 0x75, (byte) 0x7E
        };

        public SerialWriter(OutputStream out) {
            this.out = out;
        }

        public void run() {            // 여기 해야될 부분 ! 버튼 누르면 바이트 배열에 적정값 넣고 여기로 보내주기.
            System.out.println("SerialWriter ");

            try {
                int c = 0;
                while ((c = System.in.read()) > 1) {
                    if (c == 49) {
                        System.out.println("Motion 감지");
                        this.out.write(motion, 0, motion.length);
                        NetRequest.Motion();
                    } else if (c == 50) {
                        System.out.println("GAS 누출");
                        this.out.write(gas_detech, 0, gas_detech.length);
                        NetRequest.Gas_Abnormal();
                    } else if (c == 51) {
                        System.out.println("GAS 정상");
                        this.out.write(gas_normal, 0, gas_normal.length);
                        NetRequest.Gas_Normal();
                    } else if (c == 52) {
                        System.out.println("DoorLock 변경");
                        this.out.write(doorLock, 0, doorLock.length);
                        if (doorlock_value) {
                            doorlock_value = true;
                            NetRequest.DoorLock_On();
                        } else {
                            doorlock_value = false;
                            NetRequest.DoorLock_Off();
                        }

                    } else if (c == 53) {
                        System.out.println("Power ON");
                        this.out.write(power_on, 0, power_on.length);
                        power_value = true;
                        NetRequest.Power_On();
                    } else if (c == 54) {
                        System.out.println("Power OFF");
                        this.out.write(power_off, 0, power_off.length);
                        power_value = false;
                        NetRequest.Power_Off();
                    } else if (c == 55) {
                        System.out.println("Dimm ON");
                        this.out.write(dimm_on, 0, dimm_on.length);
                        dimm_value = true;
                        NetRequest.Dimm_On();
                    } else if (c == 56) {
                        System.out.println("Dimm OFF");
                        this.out.write(dimm_off, 0, dimm_off.length);
                        dimm_value = false;
                        NetRequest.Dimm_Off();
                    } else if (c == 57) {
                        System.out.println("Dimm Up");
                        this.out.write(dimm_up, 0, dimm_up.length);
                        if (dimm_level > 1) {
                            dimm_level--;
                            NetRequest.Dimm_Up();
                        }
                    } else if (c == 48) {
                        System.out.println("Dimm Down");
                        this.out.write(dimm_down, 0, dimm_down.length);
                        if (dimm_level < 20) {
                            dimm_level++;
                            NetRequest.Dimm_Down();
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static class NetRequest {
        public static final String led_on = "http://hoyuo.me:3000/light_on";
        public static final String led_off = "http://hoyuo.me:3000/light_off";

        public static void Motion() throws IOException {
            NET("http://hoyuo.me:3000/motionDetection");
        }

        public static void Gas_Normal() throws IOException {
            NET("http://hoyuo.me:3000/gas_normal");
        }

        public static void Gas_Abnormal() throws IOException {
            NET("http://hoyuo.me:3000/gas_abnormal");
        }

        public static void DoorLock_On() throws IOException {
            NET("http://hoyuo.me:3000/door_on");
        }

        public static void DoorLock_Off() throws IOException {
            NET("http://hoyuo.me:3000/door_off");
        }

        public static void Power_On() throws IOException {
            NET("http://hoyuo.me:3000/power_on");
        }

        public static void Power_Off() throws IOException {
            NET("http://hoyuo.me:3000/power_off");
        }

        public static void Dimm_On() throws IOException {
            NET("http://hoyuo.me:3000/dimm_on");
        }

        public static void Dimm_Off() throws IOException {
            NET("http://hoyuo.me:3000/dimm_off");
        }

        public static void Dimm_Up() throws IOException {
            NET("http://hoyuo.me:3000/dimm/up");
        }

        public static void Dimm_Down() throws IOException {
            NET("http://hoyuo.me:3000/dimm/down");
        }

        private static void NET(String url) throws IOException {
            URL getUrl = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
            connection.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));

            String lines;

            while ((lines = reader.readLine()) != null) {
                System.out.println(lines);
            }
            reader.close();
            connection.disconnect();
        }
    }

    public static class ServerPolling implements Runnable {
        OutputStream out;

        byte[] doorLock = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x06, (byte) 0x20, (byte) 0x09, (byte) 0x1F,
                (byte) 0x02, (byte) 0x0E, (byte) 0x02, (byte) 0xF4,
                (byte) 0x01, (byte) 0xC0, (byte) 0xB6, (byte) 0x7E
        };

        byte[] power_off = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0x3D,
                (byte) 0x02, (byte) 0x0D, (byte) 0x00, (byte) 0xF4,
                (byte) 0x53, (byte) 0x7E
        };

        byte[] power_on = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x06, (byte) 0x20, (byte) 0x09, (byte) 0x3D,
                (byte) 0x02, (byte) 0x0E, (byte) 0x02, (byte) 0xFF,
                (byte) 0xFF, (byte) 0xA3, (byte) 0xDA, (byte) 0x7E
        };

        byte[] dimm_down = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x15, (byte) 0x00, (byte) 0xD5,
                (byte) 0x81, (byte) 0x7E
        };

        byte[] dimm_up = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x16, (byte) 0x00, (byte) 0x86,
                (byte) 0xD4, (byte) 0x7E
        };

        byte[] dimm_off = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x71, (byte) 0x00, (byte) 0x3B,
                (byte) 0x46, (byte) 0x7E
        };

        byte[] dimm_on = {
                (byte) 0x7E, (byte) 0x44, (byte) 0x00, (byte) 0x00,
                (byte) 0xFF, (byte) 0xFF, (byte) 0x00, (byte) 0x00,
                (byte) 0x04, (byte) 0x20, (byte) 0x09, (byte) 0xB5,
                (byte) 0x02, (byte) 0x70, (byte) 0x00, (byte) 0x0A,
                (byte) 0x75, (byte) 0x7E
        };

        ServerPolling(OutputStream out) {
            this.out = out;
            doorlock_value = false;
            power_value = false;
            dimm_value = false;
            dimm_level = 20;
        }

        @Override
        public void run() {

            try {
                while (true) {
                    URL getUrl = new URL("http://hoyuo.me:3000");
                    HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();
                    connection.connect();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf-8"));

                    String lines;
                    lines = reader.readLine();

                    JSONParser jsonParser = new JSONParser();
                    JSONArray dataArray = (JSONArray) jsonParser.parse(lines);

                    boolean temp;

                    JSONObject data_power = (JSONObject) dataArray.get(1);
                    temp = (boolean) data_power.get("power");
                    if (temp != power_value) {

                        if (temp) {
                            this.out.write(power_on, 0, power_on.length);
                        } else {
                            this.out.write(power_off, 0, power_off.length);
                        }

                        power_value = temp;
                    }

                    JSONObject data_dimm = (JSONObject) dataArray.get(2);
                    temp = (boolean) data_dimm.get("dimm");
                    if (temp != dimm_value) {
                        dimm_value = temp;

                        if (temp) {
                            System.out.println("on");
                            this.out.write(dimm_on, 0, dimm_on.length);
                            dimm_level = 1;
                        } else {
                            System.out.println("off");
                            this.out.write(dimm_off, 0, dimm_off.length);
                            dimm_level = 20;
                        }
                    }

                    JSONObject data_doorLock = (JSONObject) dataArray.get(3);
                    temp = (boolean) data_doorLock.get("doorLock");
                    if (temp != doorlock_value) {
                        doorlock_value = temp;

                        this.out.write(doorLock, 0, doorLock.length);
                    }

                    JSONObject data_dimm_level = (JSONObject) dataArray.get(4);
                    long temp_l = (long) data_dimm_level.get("dimmLevel");
                    int temp_i = (int) temp_l;
                    if (temp_i != dimm_level) {
                        if (temp_i > dimm_level) {
                            this.out.write(dimm_down, 0, dimm_down.length);
                        } else {
                            this.out.write(dimm_up, 0, dimm_up.length);
                        }
                        dimm_level = temp_i;
                    }

                    reader.close();
                    connection.disconnect();

                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
